-- noinspection SqlNoDataSourceInspectionForFile


DROP TABLE IF EXISTS rental;
DROP TABLE IF EXISTS film_instance;
DROP TABLE IF EXISTS store;
DROP TABLE IF EXISTS film;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS film_type;

CREATE TABLE film_type (
  id            TINYINT NOT NULL,
  price_per_day DECIMAL(12, 3),
  bonus_days    SMALLINT,
  bonus_points  SMALLINT,
  PRIMARY KEY (id)
);

CREATE TABLE customer (
  id        BIGINT       NOT NULL AUTO_INCREMENT,
  full_name VARCHAR(256) NOT NULL,
  points    INT          NOT NULL DEFAULT 0,
  valid     BOOLEAN      NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id)
);

CREATE TABLE film (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  type         TINYINT      NOT NULL,
  release_date DATETIME,
  title        VARCHAR(256) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (type)
  REFERENCES film_type (id)
);

CREATE TABLE store (
  id      BIGINT NOT NULL AUTO_INCREMENT,
  name    VARCHAR(256),
  address VARCHAR(256),
  PRIMARY KEY (id)
);

CREATE TABLE film_instance (
  id       BIGINT NOT NULL AUTO_INCREMENT,
  store_id BIGINT NOT NULL,
  film_id  BIGINT NOT NULL,
  available BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id),
  FOREIGN KEY (store_id)
  REFERENCES store (id),
  FOREIGN KEY (film_id)
  REFERENCES film (id)
);

CREATE TABLE rental (
  id               BIGINT         NOT NULL AUTO_INCREMENT,
  film_instance_id BIGINT         NOT NULL,
  customer_id      BIGINT         NOT NULL,
  start_date       DATETIME       NOT NULL,
  end_date         DATETIME       NOT NULL,
  upfront_price    DECIMAL(12, 3) NOT NULL,
  returned         BOOLEAN        NOT NULL DEFAULT FALSE,
  surcharge        DECIMAL(12, 3) NOT NULL DEFAULT 0,
  days_exceeded    SMALLINT       NOT NULL DEFAULT 0,
  film_type        TINYINT        NOT NULL,
  price_per_day    DECIMAL(12, 3) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customer (id),
  FOREIGN KEY (film_instance_id) REFERENCES film_instance (id),
  FOREIGN KEY (film_type) REFERENCES film_type (id)
);

INSERT INTO customer (full_name) VALUES ('Neil Armstrong');
INSERT INTO customer (full_name) VALUES ('Charles Darwin');

INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (1,30,4,1);
INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (2,30,2,1);
INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (3,40,0,2);

INSERT INTO store (id,name,address) VALUES (1,'Central','Forever Road 777');

INSERT INTO film (title,type) VALUES ('Matrix 11',1);
INSERT INTO film (title,type) VALUES ('Spider Man',2);
INSERT INTO film (title,type) VALUES ('Spider Man 2',2);
INSERT INTO film (title,type) VALUES ('Out of Africa',3);

INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Matrix 11';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Spider Man';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Spider Man 2';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Out of Africa';