package com.casumo.rentaldemo;

import com.casumo.rentaldemo.config.MyBatisConfig;
import com.casumo.rentaldemo.dto.RentalRequest;
import com.casumo.rentaldemo.dto.ReturnRequest;
import com.casumo.rentaldemo.model.FilmType;
import com.casumo.rentaldemo.model.Rental;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.casumo.rentaldemo.exception.ErrorCodes.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentalDemoApplication.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RentalDemoApplication.class,
        MyBatisConfig.class})

public class RentalAPIIntegrationTest {

    private final static String URL = "/film-rental-api/v1/rental";
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON,
            Charset.forName("utf8"));
    private MockMvc mockMvc;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MappingJackson2HttpMessageConverter jackson2HttpMessageConverter;

    @Value("classpath:db-init-h2.sql")
    private Resource schemaScript;

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
        val populator = new ResourceDatabasePopulator();
        populator.addScript(schemaScript);
        populator.execute(dataSource);
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testRentalOK() throws Exception {
        //first we try renting normally
        RentalRequest rentalRequest = createValidRentalRequest();
        rentFilmAndTestTotal(rentalRequest, 410.0);
        //we test customer points as well
        assertEquals(5, getCustomerPoints(rentalRequest.getCustomerId()));
        List<Rental> rentals = getRentals();
        assertEquals(4, rentals.size());
        for (Rental rental : rentals) {
            switch ((int) rental.getFilmInstanceId()) {
                case 1:
                    assertEquals(new BigDecimal("30.000"), rental.getUpfrontPrice());
                    break;
                case 2:
                    assertEquals(new BigDecimal("90.000"), rental.getUpfrontPrice());
                    break;
                case 3:
                    assertEquals(new BigDecimal("90.000"), rental.getUpfrontPrice());
                    break;
                case 4:
                    assertEquals(new BigDecimal("200.000"), rental.getUpfrontPrice());
                    break;
            }
        }
        //then we try renting the same film instances again (this shouldn't happen normally)
        mockMvc.perform(post(URL + "/rent")
                .contentType(contentType)
                .content(json(rentalRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total", is(0)))
                .andExpect(jsonPath("$.errors[0].code", is((int) FILM_ALREADY_RENTED_ID)))
                .andExpect(jsonPath("$.errors[0].message", is("Film already rented")))
                .andExpect(jsonPath("$.errors[1].code", is((int) FILM_ALREADY_RENTED_ID)))
                .andExpect(jsonPath("$.errors[1].message", is("Film already rented")))
                .andExpect(jsonPath("$.errors[2].code", is((int) FILM_ALREADY_RENTED_ID)))
                .andExpect(jsonPath("$.errors[2].message", is("Film already rented")))
                .andExpect(jsonPath("$.errors[3].code", is((int) FILM_ALREADY_RENTED_ID)))
                .andExpect(jsonPath("$.errors[3].message", is("Film already rented")))
                .andExpect(jsonPath("$.prices", empty()));
        //then we try renting different films to check that they don't overlap
        clearRentals();
        rentalRequest.setFilmInstances(Arrays.asList(1L, 2L));
        rentFilmAndTestTotal(rentalRequest, 120.0);
        rentals = getRentals();
        assertEquals(2, rentals.size());
        rentalRequest.setFilmInstances(Arrays.asList(3L, 4L));
        rentFilmAndTestTotal(rentalRequest, 290.0);
        rentals = getRentals();
        assertEquals(4, rentals.size());
    }

    @Test
    public void testRentAndReturnMultipleTimes() throws Exception {
        RentalRequest rentalRequest = createValidRentalRequest();
        ReturnRequest returnRequest = createValidReturnRequest();
        //rent and return 3 times the same movies
        for (int i = 0; i < 3; i++) {
            rentFilmAndTestTotal(rentalRequest, 410.0);
            returnFilmAndTestTotal(returnRequest, 0);
        }
    }

    @Test
    public void testRentalValidationErrors() throws Exception {
        //we test sending invalid requests to provoke bad request responses
        //then we test each error code
        RentalRequest rentalRequest = createValidRentalRequest();
        rentalRequest.setCustomerId(32);
        rentFilmAndTestError(rentalRequest, INVALID_CUSTOMER);
        rentalRequest = createValidRentalRequest();
        rentalRequest.setDays((short) -1);
        rentFilmAndTestError(rentalRequest, INVALID_DAYS);
        rentalRequest = createValidRentalRequest();
        rentalRequest.setFilmInstances(new ArrayList<>());
        rentFilmAndTestError(rentalRequest, NO_FILMS_TO_RENT);
        List<Rental> rentals = getRentals();
        assertTrue(rentals.isEmpty());
    }

    @Test
    public void testReturnOK() throws Exception {
        //rent some movies
        RentalRequest rentalRequest = createValidRentalRequest();
        rentFilmAndTestTotal(rentalRequest, 410.0);
        //return them normally (no surcharge should be applied)
        ReturnRequest returnRequest = createValidReturnRequest();
        returnFilmAndTestTotal(returnRequest, 0);
        //try returning the same films again
        List<Rental> rentals = getRentals();
        assertEquals(4, rentals.size());
        for (Rental rental : rentals) {
            assertEquals(true, rental.isReturned());
        }
        mockMvc.perform(post(URL + "/return")
                .contentType(contentType)
                .content(json(returnRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.errors[0].code", is((int) FILM_ALREADY_RETURNED_ID)))
                .andExpect(jsonPath("$.errors[0].message", is("Film already returned")))
                .andExpect(jsonPath("$.errors[1].code", is((int) FILM_ALREADY_RETURNED_ID)))
                .andExpect(jsonPath("$.errors[1].message", is("Film already returned")))
                .andExpect(jsonPath("$.errors[2].code", is((int) FILM_ALREADY_RETURNED_ID)))
                .andExpect(jsonPath("$.errors[2].message", is("Film already returned")))
                .andExpect(jsonPath("$.errors[3].code", is((int) FILM_ALREADY_RETURNED_ID)))
                .andExpect(jsonPath("$.errors[3].message", is("Film already returned")))
                .andExpect(jsonPath("$.prices", empty()));
        //rent and set the dates back to have a surcharge
        clearRentals();
        rentalRequest = createValidRentalRequest();
        rentalRequest.setDays((short) 1);
        rentFilmAndTestTotal(rentalRequest, 130.0);
        setRentalDatesNDaysBack(4);
        returnFilmAndTestTotal(returnRequest, 390.0);
    }

    @Test
    public void testReturnValidationErrors() throws Exception {
        //we test sending an invalid request to provoke a bad request response
        //then we test the error code
        ReturnRequest returnRequest = createValidReturnRequest();
        returnRequest.setFilmInstances(new ArrayList<>());
        mockMvc.perform(post(URL + "/return")
                .contentType(contentType)
                .content(json(returnRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code", is((int) NO_FILMS_TO_RETURN)));
    }

    @Test
    public void testQuery() throws Exception {
        //rent some movies
        RentalRequest rentalRequest = createValidRentalRequest();
        rentFilmAndTestTotal(rentalRequest, 410.0);
        //naked query should return all rental instances
        mockMvc.perform(get(URL)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[1].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[2].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[3].customerId", is((int)rentalRequest.getCustomerId())));
        //a customer with no rentals should have... surprise! no rentals
        mockMvc.perform(get(URL+"?customerId=2")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
        //we have only one store, it should return all rentals
        mockMvc.perform(get(URL+"?storeId=1")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[1].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[2].customerId", is((int)rentalRequest.getCustomerId())))
                .andExpect(jsonPath("$[3].customerId", is((int)rentalRequest.getCustomerId())));
        //an invalid store should return no rentals
        mockMvc.perform(get(URL+"?storeId=2")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
        //returning the films and querying again should return no rentals
        ReturnRequest returnRequest = createValidReturnRequest();
        returnFilmAndTestTotal(returnRequest, 0);
        mockMvc.perform(get(URL)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    private void rentFilmAndTestError(RentalRequest rentalRequest, short errorCode) throws Exception {
        mockMvc.perform(post(URL + "/rent")
                .contentType(contentType)
                .content(json(rentalRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code", is((int) errorCode)));
    }

    private void rentFilmAndTestTotal(RentalRequest rentalRequest, double expectedAmount) throws Exception {
        mockMvc.perform(post(URL + "/rent")
                .contentType(contentType)
                .content(json(rentalRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total", is(expectedAmount)))
                .andExpect(jsonPath("$.errors", empty()));
    }

    private void returnFilmAndTestTotal(ReturnRequest returnRequest, double expectedAmount) throws Exception {
        mockMvc.perform(post(URL + "/return")
                .contentType(contentType)
                .content(json(returnRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total", is(expectedAmount)))
                .andExpect(jsonPath("$.errors", empty()));
    }

    private int getCustomerPoints(long customerId) {
        List<Integer> integerList = jdbcTemplate.query("SELECT points FROM customer WHERE id=" + customerId,
                (r, i) -> r.getInt("points"));
        assertTrue(integerList.size() == 1);
        return integerList.get(0);
    }

    private void setRentalDatesNDaysBack(int daysBack) {
        jdbcTemplate.execute("UPDATE rental SET start_date=DATEADD('DAY',-" + daysBack +
                ", start_date), end_date=DATEADD('DAY',-" + daysBack + ", end_date)");
    }

    private void clearRentals() {
        jdbcTemplate.execute("DELETE FROM rental");
        jdbcTemplate.execute("UPDATE film_instance SET available=1");
    }

    private List<Rental> getRentals() {
        return jdbcTemplate.query("SELECT * FROM rental",
                (row, i) -> {
                    return new Rental(
                            row.getLong("id"),
                            row.getLong("customer_id"),
                            row.getLong("film_instance_id"),
                            convertDate(row.getTimestamp("start_date")),
                            convertDate(row.getTimestamp("end_date")),
                            row.getBigDecimal("upfront_price"),
                            row.getBigDecimal("surcharge"),
                            row.getShort("days_exceeded"),
                            row.getBoolean("returned"),
                            row.getBigDecimal("price_per_day"),
                            getFilmType(row.getByte("film_type"))
                    );
                });
    }

    private FilmType getFilmType(byte type) {
        return jdbcTemplate.query("SELECT * FROM film_type WHERE id=" + type,
                (row, i) -> {
                    return new FilmType(
                            row.getByte("id"),
                            row.getBigDecimal("price_per_day"),
                            row.getShort("bonus_days"),
                            row.getShort("bonus_points")
                    );
                }).get(0);
    }

    private LocalDateTime convertDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    private RentalRequest createValidRentalRequest() {
        val rentalRequest = new RentalRequest();
        rentalRequest.setCustomerId(1L);
        rentalRequest.setDays((short) 5);
        rentalRequest.setFilmInstances(Arrays.asList(1L, 2L, 3L, 4L));
        return rentalRequest;
    }

    private ReturnRequest createValidReturnRequest() {
        val returnRequest = new ReturnRequest();
        returnRequest.setFilmInstances(Arrays.asList(1L, 2L, 3L, 4L));
        return returnRequest;
    }

    private String json(Object element) throws IOException {
        val mockHttpOutputMessage = new MockHttpOutputMessage();
        jackson2HttpMessageConverter.write(element, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
