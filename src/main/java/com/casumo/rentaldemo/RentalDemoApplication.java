package com.casumo.rentaldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentalDemoApplication.class, args);
	}
}
