package com.casumo.rentaldemo.controller;

import com.casumo.rentaldemo.dto.RentalRequest;
import com.casumo.rentaldemo.dto.RequestResult;
import com.casumo.rentaldemo.dto.ReturnRequest;
import com.casumo.rentaldemo.exception.*;
import com.casumo.rentaldemo.model.Rental;
import com.casumo.rentaldemo.service.CustomerService;
import com.casumo.rentaldemo.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/film-rental-api/v1/rental")
public class RentalController {

    private final RentalService rentalService;

    private final CustomerService customerService;

    @Autowired
    public RentalController(RentalService rentalService, CustomerService customerService) {
        this.rentalService = rentalService;
        this.customerService = customerService;
    }

    @GetMapping
    public ResponseEntity<List<Rental>> getNotReturnedByCustomerIdAndStoreId(
            @RequestParam(value = "customerId", required = false) Long customerId,
            @RequestParam(value = "storeId", required = false) Long storeId) {
        //both parameters are optional, the persistance layer will handle it
        return ResponseEntity.ok(rentalService.getNotReturnedByCustomerAndStore(customerId,storeId));
    }

    @PostMapping("/rent")
    public ResponseEntity<RequestResult> rentMovies(@RequestBody RentalRequest rentalRequest) {
        validateRentalRequest(rentalRequest);
        return ResponseEntity.ok(rentalService.rentFilms(rentalRequest));
    }

    @PostMapping("/return")
    public ResponseEntity<RequestResult> returnMovies(@RequestBody ReturnRequest returnRequest) {
        validateReturnRequest(returnRequest);
        return ResponseEntity.ok(rentalService.returnFilms(returnRequest));
    }

    private void validateRentalRequest(RentalRequest rentalRequest) {
        if (rentalRequest.getDays() <= 0)
            throw new InvalidDaysException();
        if (rentalRequest.getFilmInstances() == null || rentalRequest.getFilmInstances().isEmpty())
            throw new NoFilmsToRentException();
        if (!customerService.isActive(rentalRequest.getCustomerId())) {
            throw new InvalidCustomerException(rentalRequest.getCustomerId());
        }
        for (Long instanceId : rentalRequest.getFilmInstances()) {
            if (instanceId == null) {
                throw new InvalidFilmInstanceException();
            }
        }
    }

    private void validateReturnRequest(ReturnRequest returnRequest) {
        if (returnRequest.getFilmInstances() == null || returnRequest.getFilmInstances().isEmpty())
            throw new NoFilmsToReturnException();
        for (Long instanceId : returnRequest.getFilmInstances()) {
            if (instanceId == null) {
                throw new InvalidFilmInstanceException();
            }
        }
    }

}
