package com.casumo.rentaldemo.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.casumo.rentaldemo.mapper")
public class MyBatisConfig {
}
