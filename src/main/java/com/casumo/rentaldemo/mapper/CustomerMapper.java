package com.casumo.rentaldemo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface CustomerMapper {

    @Update("UPDATE customer SET points = points + #{points} WHERE id = ${id}")
    void addPoints(@Param("id") long id, @Param("points") short points);

    @Select("SELECT EXISTS(SELECT id FROM customer WHERE id=#{id} AND valid=1) AS valid;")
    boolean isActive(@Param("id") long id);
}
