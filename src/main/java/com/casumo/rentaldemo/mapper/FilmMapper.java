package com.casumo.rentaldemo.mapper;

import com.casumo.rentaldemo.model.Film;
import com.casumo.rentaldemo.model.FilmType;
import org.apache.ibatis.annotations.*;

@Mapper
public interface FilmMapper {

    @Select("SELECT * FROM film WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "releaseDate", column = "release_date"),
            @Result(property = "filmType", column = "type", javaType = FilmType.class,
                    one = @One(select = "com.casumo.rentaldemo.mapper.FilmTypeMapper.findOne")),
    })
    Film findOne(@Param("id") long id);


}
