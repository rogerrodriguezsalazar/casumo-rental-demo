package com.casumo.rentaldemo.mapper;

import com.casumo.rentaldemo.model.FilmType;
import org.apache.ibatis.annotations.*;

@Mapper
public interface FilmTypeMapper {

    @Select("SELECT * FROM film_type WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "pricePerDay", column = "price_per_day"),
            @Result(property = "bonusDays", column = "bonus_days"),
            @Result(property = "bonusPoints", column = "bonus_points")
    })
    FilmType findOne(@Param("id") long id);

}
