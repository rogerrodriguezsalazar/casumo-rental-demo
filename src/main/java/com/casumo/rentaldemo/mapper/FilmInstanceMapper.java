package com.casumo.rentaldemo.mapper;

import com.casumo.rentaldemo.model.Film;
import com.casumo.rentaldemo.model.FilmInstance;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;

@Mapper
public interface FilmInstanceMapper {

    @Select({"<script>",
            "SELECT id,store_id,film_id FROM film_instance WHERE id IN",
            "<foreach item='id' index='index' collection='ids'",
            "open='(' separator=',' close=')'>",
            "#{id}",
            "</foreach>",
            "AND available=1",
            "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "storeId", column = "store_id"),
            @Result(property = "film", column = "film_id", javaType = Film.class,
                    one = @One(select = "com.casumo.rentaldemo.mapper.FilmMapper.findOne"))
    }
    )
    List<FilmInstance> getNotRentedInstances(@Param("ids") Set<Long> ids);

    @Update("UPDATE film_instance SET available=1 WHERE id=#{filmInstanceId} AND available=0")
    int markAvailable(@Param("filmInstanceId") long filmInstanceId);

    @Update("UPDATE film_instance SET available=0 WHERE id=#{filmInstanceId} AND available=1")
    int markUnavailable(@Param("filmInstanceId") long filmInstanceId);
}
