package com.casumo.rentaldemo.mapper;

import com.casumo.rentaldemo.model.FilmType;
import com.casumo.rentaldemo.model.Rental;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Mapper
public interface RentalMapper {

    @Insert("INSERT INTO rental (film_instance_id,customer_id,start_date,end_date,upfront_price," +
            "film_type,price_per_day)" +
            "VALUES (#{rental.filmInstanceId},#{rental.customerId},#{rental.startDate},#{rental.endDate}," +
            "#{rental.upfrontPrice},#{rental.filmType.id},#{rental.pricePerDay})")
    @Options(useGeneratedKeys = true, keyProperty = "rental.id", keyColumn = "id")
    void create(@Param("rental") Rental rental);

    @Update("UPDATE rental SET returned=1,surcharge=#{rental.surcharge}," +
            "days_exceeded=#{rental.daysExceeded} WHERE id=#{rental.id} AND returned=0")
    int returnFilm(@Param("rental") Rental rental);

    @Select({
            "<script>",
            "SELECT id,film_instance_id,end_date,price_per_day,film_type from rental WHERE film_instance_id IN",
            "<foreach item='id' index='index' collection='ids'",
            "open='(' separator=',' close=')'>",
            "#{id}",
            "</foreach>",
            "AND returned=0",
            "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "filmInstanceId", column = "film_instance_id"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "pricePerDay", column = "price_per_day"),
            @Result(property = "filmType", column = "film_type", javaType = FilmType.class,
                    one = @One(select = "com.casumo.rentaldemo.mapper.FilmTypeMapper.findOne"))
    })
    List<Rental> findRentedByFilmInstanceIds(@Param("ids") Set<Long> filmInstanceIds);

    @Select({"<script>",
            "SELECT rental.id as id,film_instance_id,customer_id,start_date,end_date,upfront_price,returned,",
            "surcharge,days_exceeded,film_type,price_per_day FROM rental ",
            "<if test=\"storeId != null\">",
            "INNER JOIN film_instance ON (film_instance.id=film_instance_id AND store_id=#{storeId}) ",
            "</if>",
            "WHERE ",
            "<if test=\"customerId != null\">",
            "customer_id = #{customerId} AND",
            "</if>",
            "returned=0",
            "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "filmInstanceId", column = "film_instance_id"),
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "upfrontPrice", column = "upfront_price"),
            @Result(property = "returned", column = "returned"),
            @Result(property = "surcharge", column = "surcharge"),
            @Result(property = "daysExceeded", column = "days_exceeded"),
            @Result(property = "pricePerDay", column = "price_per_day"),
            @Result(property = "filmType", column = "film_type", javaType = FilmType.class,
                    one = @One(select = "com.casumo.rentaldemo.mapper.FilmTypeMapper.findOne"))
    })
    List<Rental> getNotReturnedByCustomerAndStore(@Param("customerId") Long customerId, @Param("storeId") Long storeId);


}
