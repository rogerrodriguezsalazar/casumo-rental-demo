package com.casumo.rentaldemo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
//not relevant fields will be ignored by jackson
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReturnRequest {

    private List<Long> filmInstances = new ArrayList<>();

}
