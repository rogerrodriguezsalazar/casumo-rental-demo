package com.casumo.rentaldemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RentalRequest {

    private long customerId;
    private short days;
    private List<Long> filmInstances = new ArrayList<>();

}
