package com.casumo.rentaldemo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

@Value
@AllArgsConstructor
@EqualsAndHashCode
//not relevant fields will be ignored by jackson
@JsonInclude(Include.NON_NULL)
public class RentalError {

    private short code;
    private String message;
    private Long rentalId;
    private Long filmInstanceId;
    private Long customerId;

}
