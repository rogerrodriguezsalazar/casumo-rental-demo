package com.casumo.rentaldemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RequestResult {

    private List<FilmPrice> prices = new ArrayList<>();
    private BigDecimal total = BigDecimal.ZERO;
    private List<RentalError> errors = new ArrayList<>();

}
