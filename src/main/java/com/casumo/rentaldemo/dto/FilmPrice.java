package com.casumo.rentaldemo.dto;

import com.casumo.rentaldemo.model.Film;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@Value
@AllArgsConstructor
@EqualsAndHashCode
//not relevant fields will be ignored by jackson
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilmPrice {

    private Film film;
    private long filmInstanceId;
    private BigDecimal price;

}
