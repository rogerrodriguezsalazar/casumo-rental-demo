package com.casumo.rentaldemo.exception;

public interface ErrorCodes {

    short INVALID_CUSTOMER = 1000;
    short INVALID_DAYS = 1001;
    short NO_FILMS_TO_RENT = 1002;
    short NO_FILMS_TO_RETURN = 1003;
    short INVALID_FILM_INSTANCE_ID = 1004;
    //this should never be used in reality
    short FILM_ALREADY_RENTED_ID = 1005;
    short FILM_ALREADY_RETURNED_ID = 1006;
    short RENTAL_CREATION_ERROR = 1007;
    short RENTAL_RETURN_ERROR = 1008;
    short MALFORMED_REQUEST = 1009;

}
