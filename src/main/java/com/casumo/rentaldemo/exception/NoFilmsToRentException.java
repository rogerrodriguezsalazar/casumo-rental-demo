package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;

public class NoFilmsToRentException extends BaseRentalException {

    public NoFilmsToRentException() {
        super(new RentalError(ErrorCodes.NO_FILMS_TO_RENT,
                "At least must 1 film must be selected for rental",
                null,
                null,
                null));
    }

}
