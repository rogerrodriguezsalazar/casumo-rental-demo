package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;
import lombok.Getter;

public abstract class BaseRentalException extends RuntimeException {

    @Getter
    private final RentalError rentalError;

    protected BaseRentalException(RentalError rentalError) {
        super(rentalError.getMessage());
        this.rentalError = rentalError;
    }

}
