package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;

public class NoFilmsToReturnException extends BaseRentalException {

    public NoFilmsToReturnException() {
        super(new RentalError(ErrorCodes.NO_FILMS_TO_RETURN,
                "At least must 1 film must be selected for return",
                null,
                null,
                null));
    }

}
