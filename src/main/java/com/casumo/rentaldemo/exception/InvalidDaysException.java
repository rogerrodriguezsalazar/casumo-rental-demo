package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;

public class InvalidDaysException extends BaseRentalException {

    public InvalidDaysException() {
        super(new RentalError(ErrorCodes.INVALID_DAYS,
                "Rental days must be >= 1",
                null,
                null,
                null));
    }
}
