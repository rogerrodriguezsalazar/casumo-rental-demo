package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;

public class InvalidFilmInstanceException extends BaseRentalException {

    public InvalidFilmInstanceException() {
        super(new RentalError(ErrorCodes.INVALID_FILM_INSTANCE_ID,
                "Invalid film code",
                null,
                null,
                null));
    }

}
