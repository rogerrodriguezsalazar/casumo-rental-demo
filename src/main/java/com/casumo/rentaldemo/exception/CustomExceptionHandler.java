package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.casumo.rentaldemo.exception.ErrorCodes.MALFORMED_REQUEST;

@RestController
@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BaseRentalException.class)
    public ResponseEntity<RentalError> handleBaseRentalException(BaseRentalException ex) {
        log.error(ex.getRentalError().getMessage(), ex);
        return ResponseEntity.badRequest().body(ex.getRentalError());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.badRequest().body(new RentalError(MALFORMED_REQUEST,
                "Malformed request: " + ex.getMessage(),
                null,
                null,
                null));
    }

}
