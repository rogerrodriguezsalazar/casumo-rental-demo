package com.casumo.rentaldemo.exception;

import com.casumo.rentaldemo.dto.RentalError;

public class InvalidCustomerException extends BaseRentalException {

    public InvalidCustomerException(long customerId) {
        super(new RentalError(ErrorCodes.INVALID_CUSTOMER,
                "Invalid customer: " + customerId,
                null,
                null,
                customerId));
    }

}
