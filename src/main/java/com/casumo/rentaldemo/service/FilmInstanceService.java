package com.casumo.rentaldemo.service;

import com.casumo.rentaldemo.mapper.FilmInstanceMapper;
import com.casumo.rentaldemo.model.FilmInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class FilmInstanceService {

    private final FilmInstanceMapper filmInstanceMapper;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public FilmInstanceService(FilmInstanceMapper filmInstanceMapper) {
        this.filmInstanceMapper = filmInstanceMapper;
    }

    public List<FilmInstance> getNotRentedInstances(Set<Long> instanceIds) {
        return filmInstanceMapper.getNotRentedInstances(instanceIds);
    }

    public boolean markAvailable(long filmInstanceId) {
        //this works a as a lock comparing and setting in the database
        //if it returns 0, it means the film instance is already available (returned)
        return filmInstanceMapper.markAvailable(filmInstanceId)>0;
    }

    public boolean markUnavailable(long filmInstanceId) {
        //this works a as a lock comparing and setting in the database
        //if it returns 0, it means the film instance is already unavailable (rented)
        return filmInstanceMapper.markUnavailable(filmInstanceId)>0;
    }

}
