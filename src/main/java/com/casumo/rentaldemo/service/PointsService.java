package com.casumo.rentaldemo.service;

import com.casumo.rentaldemo.model.Rental;
import org.springframework.stereotype.Service;

@Service
public class PointsService {

    public short getPointsForRental(Rental rental) {
        return rental.getFilmType().getBonusPoints();
    }

}
