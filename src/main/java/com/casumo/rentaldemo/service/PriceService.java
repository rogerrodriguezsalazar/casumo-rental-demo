package com.casumo.rentaldemo.service;

import com.casumo.rentaldemo.model.Rental;
import lombok.val;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class PriceService {

    public void calcualteAndSetUpfrontPrice(Rental rental) {
        val pricePerDay = rental.getFilmType().getPricePerDay();
        int days = getRentDays(rental);
        rental.setUpfrontPrice(pricePerDay.multiply(new BigDecimal(days)));
    }

    public void calculateAndSetSurcharge(Rental rental) {
        val pricePerDay = rental.getFilmType().getPricePerDay();
        short days = getDaysExceeded(rental);
        rental.setDaysExceeded(days);
        rental.setSurcharge(pricePerDay.multiply(new BigDecimal(days)));
    }

    private int getRentDays(Rental rental) {
        int days = (int) Duration.between(rental.getStartDate(), rental.getEndDate()).toDays();
        days -= rental.getFilmType().getBonusDays();
        if (days <= 0) {
            days = 1;
        }
        return days;
    }

    private short getDaysExceeded(Rental rental) {
        short days = (short) Duration.between(rental.getEndDate(), LocalDateTime.now()).toDays();
        return days >= 0 ? days : 0;
    }


}
