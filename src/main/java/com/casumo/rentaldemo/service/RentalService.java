package com.casumo.rentaldemo.service;

import com.casumo.rentaldemo.dto.*;
import com.casumo.rentaldemo.exception.ErrorCodes;
import com.casumo.rentaldemo.mapper.RentalMapper;
import com.casumo.rentaldemo.model.FilmInstance;
import com.casumo.rentaldemo.model.Rental;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RentalService {

    private final RentalMapper rentalMapper;
    private final FilmInstanceService filmInstanceService;
    private final CustomerService customerService;
    private final PriceService priceService;
    private final PointsService pointsService;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public RentalService(RentalMapper rentalMapper,
                         FilmInstanceService filmInstanceService,
                         CustomerService customerService,
                         PriceService priceService, PointsService pointsService) {
        this.rentalMapper = rentalMapper;
        this.filmInstanceService = filmInstanceService;
        this.customerService = customerService;
        this.priceService = priceService;
        this.pointsService = pointsService;
    }

    public List<Rental> getNotReturnedByCustomerAndStore(Long customerId, Long storeId) {
        return rentalMapper.getNotReturnedByCustomerAndStore(customerId, storeId);
    }

    public RequestResult returnFilms(ReturnRequest returnRequest) {
        val requestResult = new RequestResult();
        //film instances to rent should be unique
        //this is to prevent intentional tampering
        Set<Long> instanceIds = new HashSet<>(returnRequest.getFilmInstances());
        val rentals = rentalMapper.findRentedByFilmInstanceIds(instanceIds);
        appendInvalidFilmInstances(instanceIds,
                getInstanceIdsFrom(rentals, o -> ((Rental) o).getFilmInstanceId()),
                requestResult, false);
        BigDecimal total = BigDecimal.ZERO;
        for (Rental rental : rentals) {
            priceService.calculateAndSetSurcharge(rental);
            try {
                if (rentalMapper.returnFilm(rental)==0) {
                    //the movie is already returned
                    appendReturnedError(requestResult, rental.getFilmInstanceId());
                    continue;
                }
                if (!filmInstanceService.markAvailable(rental.getFilmInstanceId())) {
                    appendReturnedError(requestResult, rental.getFilmInstanceId());
                    //the movie is already returned, yet for some reason the rental status is not updated
                    continue;
                }
                log.info("movie instance {} successfully returned", rental.getFilmInstanceId());
                total = total.add(rental.getSurcharge());
                requestResult.getPrices().add(
                        new FilmPrice(null, rental.getFilmInstanceId(), rental.getSurcharge()));
            } catch (Exception exception) {
                //we can still attempt to return the other movies
                appendReturnError(requestResult, rental.getFilmInstanceId(), exception);
            }
        }
        requestResult.setTotal(total);
        return requestResult;
    }

    private Set<Long> getInstanceIdsFrom(List<?> availableList, Function<Object, Long> function) {
        //it's faster to put them into a set, access=O(1)
        return availableList.stream().map(function::apply).collect(Collectors.toSet());
    }

    public RequestResult rentFilms(RentalRequest rentalRequest) {
        val requestResult = new RequestResult();
        //film instances to rent should be unique
        //this is to prevent intentional tampering
        Set<Long> instanceIds = new HashSet<>(rentalRequest.getFilmInstances());
        val instancesToRentList = filmInstanceService.getNotRentedInstances(instanceIds);
        appendInvalidFilmInstances(instanceIds,
                getInstanceIdsFrom(instancesToRentList, o -> ((FilmInstance) o).getId()),
                requestResult, true);
        //calculate dates from days
        val startDate = LocalDateTime.now();
        val endDate = startDate.plusDays(rentalRequest.getDays());
        short bonusPoints = 0;
        BigDecimal total = BigDecimal.ZERO;
        for (FilmInstance filmInstance : instancesToRentList) {
            if (!filmInstanceService.markUnavailable(filmInstance.getId())) {
                appendRentedError(requestResult, filmInstance.getId());
                //the movie is already rented
                continue;
            }
            Rental rental = buildRental(filmInstance, rentalRequest.getCustomerId(), startDate, endDate);
            try {
                rentalMapper.create(rental);
                log.info("rental {} created successfully for film instance {}", rental.getId(), filmInstance.getId());
                total = total.add(rental.getUpfrontPrice());
                bonusPoints += pointsService.getPointsForRental(rental);
                requestResult.getPrices().add(
                        new FilmPrice(filmInstance.getFilm(), filmInstance.getId(), rental.getUpfrontPrice())
                );
            } catch (Exception exception) {
                //we can still attempt to rent the other movies
                appendRentError(requestResult, filmInstance.getId(), exception);
            }
        }
        requestResult.setTotal(total);
        if (bonusPoints > 0) {
            customerService.addPoints(rentalRequest.getCustomerId(), bonusPoints);
        }
        return requestResult;
    }

    private Rental buildRental(FilmInstance filmInstance,
                               long customerId,
                               LocalDateTime startDate,
                               LocalDateTime endDate) {
        val rental = new Rental();
        rental.setStartDate(startDate);
        rental.setEndDate(endDate);
        val filmType = filmInstance.getFilm().getFilmType();
        rental.setFilmType(filmType);
        rental.setCustomerId(customerId);
        rental.setPricePerDay(filmType.getPricePerDay());
        rental.setFilmInstanceId(filmInstance.getId());
        priceService.calcualteAndSetUpfrontPrice(rental);
        return rental;
    }

    private void appendInvalidFilmInstances(Set<Long> instanceIds,
                                            Set<Long> availableIds,
                                            RequestResult requestResult,
                                            boolean renting) {
        for (Long instanceId : instanceIds) {
            //this should not be necessary as each film instance is unique
            //but we need to check it to prevent unintended consequences
            //it may happen when trying to rent the same film twice without returning it
            //or when trying to return the same film twice without renting it
            if (!availableIds.contains(instanceId)) {
                if (renting) {
                    appendRentedError(requestResult, instanceId);
                } else {
                    appendReturnedError(requestResult, instanceId);
                }
            }
        }
    }

    private void appendReturnError(RequestResult requestResult, long filmInstanceId, Exception exception) {
        log.error(exception.getMessage(), exception);
        requestResult.getErrors().add(
                new RentalError(
                        ErrorCodes.RENTAL_RETURN_ERROR,
                        "There was an error trying to return the film: " + exception.getMessage(),
                        filmInstanceId,
                        null,
                        null
                )
        );
    }

    private void appendReturnedError(RequestResult requestResult, Long instanceId) {
        requestResult.getErrors().add(new RentalError(ErrorCodes.FILM_ALREADY_RETURNED_ID,
                "Film already returned",
                null,
                instanceId,
                null));
        log.error("film instance {} already returned", instanceId);
    }

    private void appendRentError(RequestResult requestResult, long filmInstanceId, Exception exception) {
        log.error(exception.getMessage(), exception);
        requestResult.getErrors().add(
                new RentalError(
                        ErrorCodes.RENTAL_CREATION_ERROR,
                        "There was an error creating the rental record: " + exception.getMessage(),
                        null,
                        filmInstanceId,
                        null
                )
        );
    }

    private void appendRentedError(RequestResult requestResult, long instanceId) {
        requestResult.getErrors().add(new RentalError(ErrorCodes.FILM_ALREADY_RENTED_ID,
                "Film already rented",
                null,
                instanceId,
                null));
        log.error("film instance {} already rented", instanceId);
    }

}
