package com.casumo.rentaldemo.service;

import com.casumo.rentaldemo.mapper.CustomerMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerService {

    private final CustomerMapper customerMapper;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public CustomerService(CustomerMapper customerMapper) {
        this.customerMapper = customerMapper;
    }

    public void addPoints(long customerId, short points) {
        customerMapper.addPoints(customerId, points);
        log.info("{} points added to customer {}", points, customerId);
    }

    public boolean isActive(long customerId) {
        return customerMapper.isActive(customerId);
    }
}
