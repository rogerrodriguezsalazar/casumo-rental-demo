package com.casumo.rentaldemo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
//not relevant fields will be ignored by jackson
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilmInstance {

    private long id;
    private long storeId;
    private Film film;

}
