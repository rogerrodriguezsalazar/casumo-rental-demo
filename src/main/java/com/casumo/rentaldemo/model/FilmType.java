package com.casumo.rentaldemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class FilmType {

    private byte id;
    private BigDecimal pricePerDay;
    private short bonusDays;
    private short bonusPoints;

}
