package com.casumo.rentaldemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Rental {

    private long id;
    private long customerId;
    private long filmInstanceId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private BigDecimal upfrontPrice;
    private BigDecimal surcharge;
    private short daysExceeded;
    private boolean returned;
    private BigDecimal pricePerDay;
    private FilmType filmType;

}
