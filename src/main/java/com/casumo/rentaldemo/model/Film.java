package com.casumo.rentaldemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
//not relevant fields will be ignored by jackson
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Film {

    private long id;
    private String title;
    private LocalDate releaseDate;
    private FilmType filmType;

}
