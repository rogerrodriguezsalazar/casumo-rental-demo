# noinspection SqlNoDataSourceInspectionForFile

CREATE SCHEMA casumo_movie_rental
  DEFAULT CHARACTER SET utf8;

USE casumo_movie_rental;

CREATE TABLE film_type (
  id            TINYINT NOT NULL,
  price_per_day DECIMAL(12, 3),
  bonus_days    SMALLINT,
  bonus_points  SMALLINT,
  PRIMARY KEY (id)
);

CREATE TABLE customer (
  id        BIGINT       NOT NULL AUTO_INCREMENT,
  full_name VARCHAR(256) NOT NULL,
  points    INT          NOT NULL DEFAULT 0,
  valid     BOOLEAN      NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id)
);

CREATE TABLE film (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  type         TINYINT      NOT NULL,
  release_date DATETIME,
  title        VARCHAR(256) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (type)
  REFERENCES film_type (id)
);

CREATE TABLE store (
  id      BIGINT NOT NULL AUTO_INCREMENT,
  name    VARCHAR(256),
  address VARCHAR(256),
  PRIMARY KEY (id)
);

CREATE TABLE film_instance (
  id       BIGINT NOT NULL AUTO_INCREMENT,
  store_id BIGINT NOT NULL,
  film_id  BIGINT NOT NULL,
  available BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id),
  FOREIGN KEY (store_id)
  REFERENCES store (id),
  FOREIGN KEY (film_id)
  REFERENCES film (id)
);

CREATE TABLE rental (
  id               BIGINT         NOT NULL AUTO_INCREMENT,
  film_instance_id BIGINT         NOT NULL,
  customer_id      BIGINT         NOT NULL,
  start_date       DATETIME       NOT NULL,
  end_date         DATETIME       NOT NULL,
  upfront_price    DECIMAL(12, 3) NOT NULL,
  returned         BOOLEAN        NOT NULL DEFAULT FALSE,
  surcharge        DECIMAL(12, 3) NOT NULL DEFAULT 0,
  days_exceeded    SMALLINT       NOT NULL DEFAULT 0,
  film_type        TINYINT        NOT NULL,
  price_per_day    DECIMAL(12, 3) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customer (id),
  FOREIGN KEY (film_instance_id) REFERENCES film_instance (id),
  FOREIGN KEY (film_type) REFERENCES film_type (id)
);
