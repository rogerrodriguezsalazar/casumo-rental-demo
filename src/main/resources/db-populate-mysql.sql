# noinspection SqlNoDataSourceInspectionForFile

USE casumo_movie_rental;

INSERT INTO customer (full_name) VALUES ('Neil Armstrong');
INSERT INTO customer (full_name) VALUES ('Charles Darwin');

INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (1,30,4,1);
INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (2,30,2,1);
INSERT INTO film_type (id,price_per_day,bonus_days,bonus_points) VALUES (3,40,0,2);

INSERT INTO store (id,name,address) VALUES (1,'Central','Forever Road 777');

INSERT INTO film (title,type) VALUES ('Matrix 11',1);
INSERT INTO film (title,type) VALUES ('Spider Man',2);
INSERT INTO film (title,type) VALUES ('Spider Man 2',2);
INSERT INTO film (title,type) VALUES ('Out of Africa',3);

INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Matrix 11';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Spider Man';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Spider Man 2';
INSERT INTO film_instance (store_id,film_id) SELECT 1,id FROM film WHERE title='Out of Africa';
